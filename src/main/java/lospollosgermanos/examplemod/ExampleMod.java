package lospollosgermanos.examplemod;

import lospollosgermanos.examplemod.item.ItemExample;
import lospollosgermanos.examplemod.proxy.ExampleCommonProxy;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = ExampleMod.MODID, version = ExampleMod.VERSION)
public class ExampleMod {

	public static final String MODID = "examplemod";

	static final String VERSION = "3.1.4.15";

	@Instance
	public static ExampleMod instance = new ExampleMod();

	@SidedProxy(serverSide="lospollosgermanos.examplemod.proxy.ExampleCommonProxy", clientSide="lospollosgermanos.examplemod.proxy.ExampleClientProxy")
	private static ExampleCommonProxy proxy;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		registerItems();
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		registerRecipes();
	}

	private void registerItems() {
		GameRegistry.register(new ItemExample());
	}

	private void registerRecipes() {
		GameRegistry.addShapedRecipe(new ItemStack(Blocks.COBBLESTONE, 3),
				"XX",
				"XX",
				'X', Blocks.GRAVEL);

		GameRegistry.addShapelessRecipe(new ItemStack(Blocks.GRAVEL, 1), Blocks.COBBLESTONE);

		GameRegistry.addSmelting(Blocks.GRAVEL, new ItemStack(Blocks.STONE, 1), 1f);
	}

	public static ExampleCommonProxy getProxy() {
		return proxy;
	}
}
