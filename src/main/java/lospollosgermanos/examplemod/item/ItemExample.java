package lospollosgermanos.examplemod.item;

import lospollosgermanos.examplemod.ExampleMod;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemExample extends Item {

	private static final String ID = "example_item";

	public ItemExample() {
		setCreativeTab(CreativeTabs.MISC);
		setUnlocalizedName(ID);
		setRegistryName(ExampleMod.MODID, ID);

		ExampleMod.getProxy().registerCustomModel(this, 0, getCustomModelLocation());
	}

	protected ModelResourceLocation getCustomModelLocation() {
		return new ModelResourceLocation(ExampleMod.MODID + ":" + ID, "inventory");
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn, EnumHand hand) {
		playerIn.addPotionEffect(new PotionEffect(MobEffects.RESISTANCE, 200, 3));
		playerIn.getCooldownTracker().setCooldown(this, 300);
		return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemStackIn);
	}
}
